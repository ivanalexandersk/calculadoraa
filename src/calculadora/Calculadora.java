/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author ivana
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        aritmetica arit;
        trigonometrica tri;
        extras ex;
        ex=new extras();
        tri=new trigonometrica();
        arit=new aritmetica();
        
        System.out.println("valores |a,b| para operaciones aritmeticas:");
        arit.a=5;
        arit.b=2;
        System.out.println("suma");
        System.out.println(arit.suma());
        System.out.println("resta");
        System.out.println(arit.resta());
        System.out.println("multiplicacion");
        System.out.println(arit.multiplicacion());
        System.out.println("division");
        System.out.println(arit.division());
        
        System.out.println("dele un valor a x para las trigo y uno a y y z para la potencia");
        tri.x=65;
        tri.y=5;
        tri.z=10;
        System.out.println("seno");
        System.out.println(tri.seno());
        System.out.println("coseno");
        System.out.println(tri.coseno());
        System.out.println("tangente");
        System.out.println(tri.tangente());
        System.out.println("potencia");
        System.out.println(tri.potencia());
        
        
        System.out.println("numero aleatorio");
        System.out.println(ex.aleatorio());
        
    }
    
}
